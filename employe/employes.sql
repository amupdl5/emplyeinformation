show databases;
use employese;
show tables;
select * from employese;
select  deptid, avg(salary) from employese group by deptid;
select salary, min(salary) from employese group by salary;
select deptid, count(salary) from employese group by deptid;
select name, max(deptid) from employese group by deptid;
select deptid, sum(salary) from employese group by deptid;
select name, sum(salary) from employese group by name;





#having syntax
select name, sum(salary)  as salary from employese group by name having sum(salary) <10000;
select deptid, sum(salary) as salary from employese group by deptid having deptid=345;
select employeeid, max(salary) as salary from employese group by employeeid having employeeid=2;
select name, sum(salary) as salary from employese group by name having name="asa";
select deptid, min(salary) as salary from employese group by deptid having deptid=345;
